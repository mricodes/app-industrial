// In App.js in a new project

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from './components/pages/Main/index.js';
import CameraScreen from './components/pages/Camera/index.js';
import WriteinfoScreen from './components/pages/Writeinfo/index.js';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Main" screenOptions={{
        headerShown: false,
        header:false,
      }}>
        <Stack.Screen name="Main" component={MainScreen} />
        <Stack.Screen name="Camera" component={CameraScreen} screenOptions={{
        headerShown: false,
      }} />
      <Stack.Screen name="Writeinfo" component={WriteinfoScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default App;