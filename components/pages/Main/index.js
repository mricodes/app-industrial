import React, { useState } from 'react';
import { Text, View, Image, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles'

let character_Pos = 0;
let barcode_info = '';
let analyst_info = 0;
let qrcode_info = '';
const barcode_Key = 'BARCODE_KEY';
const qrcode_Key = 'QRCODE_KEY';
const analyst_Key = 'ANALYST_KEY';
let Info = [];




const images = {
    char: {
        a0: require('../../../src/images/AutoLab_generic.png'),
        a1: require('../../../src/images/config.png'),
        a2: require('../../../src/images/sample.png'),
    }
}
const image_list = [
    images.char.a0,
    images.char.a1,
    images.char.a2,
]

getData = async (key) => {
    try {
        const value = await AsyncStorage.getItem(key);
        if (value !== null) {
            // value previously stored
            switch (key) {
                case qrcode_Key:
                    qrcode_info = value;
                    console.log(qrcode_info);
                    break;
                case barcode_Key:
                    barcode_info = value;
                    console.log(barcode_info);
                    break;
                case analyst_Key:
                    analyst_info = value;
                    break;
            }
        } else {
            switch (key) {
                case qrcode_Key:
                    console.log('Sem dados QRCODE');
                    break;
                case barcode_Key:
                    console.log('SEM DADOS CODIGO DE BARRAS');
                    break;
                case analyst_Key:
                    analyst_info = value;
                    break;
            }
        }
    } catch (e) {
        alert(e);
        // error reading value
    }
}

const storeData = async (key, value) => {
    try {
        await AsyncStorage.setItem(key, value)
        console.log(`saveData: ${value}`)
    } catch (e) {
        // saving error
    }
}
function MainScreen({ navigation }) {
        getData(qrcode_Key);
        getData(barcode_Key);
        getData(analyst_Key);


    const [statusInfo, setstatusInfo] = useState(false);
    const [Barcode, setBarcode] = useState(barcode_info);
    const [Qrcode, setQrcode] = useState(qrcode_info);
    const [Analyst, setAnalyst] = useState(analyst_info);
    return (
        <View style={styles.container}>
            <Image style={styles.img_logo}
                source={image_list[0]}
            ></Image>
            <TouchableOpacity style={styles.but_cam} activeOpacity={.8} onPress={() => navigation.navigate('Camera')}>
                <Image style={styles.img_sample}
                    source={image_list[2]}
                ></Image>
                <Text style={styles.but_text}>Coletar Amostra</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.but_cam} activeOpacity={.8} onPress={() => (
                setBarcode(barcode_info),
                console.log(barcode_info),
                setAnalyst(Analyst + 1),
                setQrcode(qrcode_info),
                console.log(qrcode_info),
                setstatusInfo(statusInfo ? false : true)
            )}>
                <Text style={styles.but_text}>Testando status</Text>
            </TouchableOpacity> */}

            <TouchableOpacity style={styles.config_but} activeOpacity={.2} >
                <Image style={styles.img_config}
                    source={image_list[1]}
                ></Image>
            </TouchableOpacity>
            {!statusInfo && <View style={styles.info_container}>
                <Text style={styles.title_container}>Informações</Text>
                <View style={styles.line_container}></View>
                <Text style={styles.infotext_container}>Id Analista:
                <Text style={styles.boldtext_container}>{Analyst}</Text></Text>
                <Text style={styles.infotext_container}>Código de Barras:
                <Text style={styles.boldtext_container}>{Barcode}</Text></Text>
                <Text style={styles.infotext_container}>Qrcode:
                <Text style={styles.boldtext_container}>{Qrcode}</Text></Text>
            </View>}
            <Text style={styles.info_margin}>Status: <Text style={statusInfo ? styles.info_statusblue : styles.info_statusred}>{statusInfo ? 'Sincronizado' : 'Não Sincronizado'}</Text></Text>
            <Text style={styles.end_text}>MRI Tecnologia Eletrônica LTDA.</Text>

        </View>
    );
}

export default MainScreen;