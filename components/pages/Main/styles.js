import { StyleSheet,Dimensions} from 'react-native';

const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 2;
export const IMAGE_HEIGHT_SMALL = window.width / 7;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    img_logo: {
        width: 300,
        height:IMAGE_HEIGHT,
        marginTop:20,
        resizeMode: 'contain'
    },
    img_config: {
        width: 50,
        height: 50,

    },
    config_but: {
        position: 'absolute',
        bottom: 20,
        right: 12,
    },
    but_cam: {
        backgroundColor: '#2063AB',
        padding: 5,
        alignItems: 'center',
        borderRadius: 30,
        justifyContent: 'space-between',
        flexDirection: 'row',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.65,

        elevation: 10,
    },
    but_text: {
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: 5,
        padding: 10,
        textAlign: 'center',
        textAlignVertical: 'center'

    },
    end_text: {
        color: '#2063AB',
        fontSize: 10,
    },
    info_container: {
        borderColor: '#2063AB',
        borderWidth: 3,
        padding: 20,
        borderRadius: 40,
        width: '90%',
        marginTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    title_container: {
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold',
        color: '#2063AB'

    },
    line_container: {
        height: 5,
        width: '100%',
        backgroundColor: '#2063AB',
        marginBottom: 10
    },
    infotext_container: {
        fontSize: 15,
        marginLeft: 15,
    },
    info_statusred: {
        //color: '#1B998B',
        color: 'red',
        fontWeight: 'bold',
        
    },
    info_statusblue: {
        color: '#1B998B',
        fontWeight: 'bold',
    },
    info_margin:{
        fontSize:20,
        marginBottom:50
    },
    boldtext_container: {
        fontWeight: 'bold'
    },
    img_sample: {
        width: 40,
        height: 40,
        tintColor: '#fff'
    },
});

export default styles;