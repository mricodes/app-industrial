import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles'


const barcode_Key = 'BARCODE_KEY';
const qrcode_Key = 'QRCODE_KEY';





function CameraScreen({ navigation }) {

    const storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(barcode_Key, value)
        } catch (e) {
            // saving error
        }
    }

    getData = async () => {
        try {
            const value = await AsyncStorage.getItem(barcode_Key)
            if (value !== null) {
                return value;
            } else {
            }
        } catch (e) {
            console.log(e);
            // error reading value
        }
    }

    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    const handleBarCodeScanned = async ({ type, data }) => {
        let teste;
        setScanned(true);
        if (type === 1) {
            storeData(barcode_Key, data);
            alert(`Código de barras: ${data}`);

        } else {
            storeData(qrcode_Key, data);
            alert(`Qr code: ${data}`);
        }

    };

    if (hasPermission === null) {
        return <Text style={styles.flip_cam}>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View style={styles.container}>

            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={styles.barcodescam}
            />
            <View style={styles.square_cam}>
            </View>
            <View style={styles.square_top}>
            </View>
            <View style={styles.square_right}>
            </View>
            <View style={styles.square_bottom}>

            </View>
            <View style={styles.square_left}>
            </View>
            {scanned &&
                <TouchableOpacity style={styles.but_cam} activeOpacity={.2} onPress={() => setScanned(false)}>
                    <Text style={styles.but_text}>Escanear Novamente</Text>
                </TouchableOpacity>}
            <TouchableOpacity style={styles.but_scan} activeOpacity={.2} onPress={() => navigation.navigate('Writeinfo')}>
                <Text style={styles.but_text}>Digitar Códigos</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.but_back} activeOpacity={.2} onPress={() => navigation.navigate('Main')}>
                <Text style={styles.but_text}>Voltar</Text>
            </TouchableOpacity>

            {scanned && <TouchableOpacity style={styles.but_check} activeOpacity={.2}>
                <Text style={styles.but_text}>Confirmar</Text>
            </TouchableOpacity>}

        </View>
    );
}

export default CameraScreen;