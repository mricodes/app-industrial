import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0,
    },
    barcodescam: {
        height: '100%',
        width: '125%',
    },
    square_cam: {
        width: '80%',
        height: '50%',
        borderColor: '#FF0000',
        borderStyle: 'dashed',
        borderWidth: 4,
        borderRadius: 1,
        position: 'absolute',
        opacity: 0.6,
        marginBottom: 30
    },
    square_top: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: '100%',
        height: '25%',
        backgroundColor: '#000',
        opacity: 0.5
    },
    square_right: {
        position: 'absolute',
        right: 0,
        top: '25%',
        width: '10%',
        height: '50%',
        backgroundColor: '#000',
        opacity: 0.5
    },
    square_bottom: {
        position: 'absolute',
        left: 0,
        bottom: 0,
        width: '100%',
        height: '25%',
        backgroundColor: '#000',
        opacity: 0.5
    },
    square_left: {
        position: 'absolute',
        left: 0,
        top: '25%',
        width: '10%',
        height: '50%',
        backgroundColor: '#000',
        opacity: 0.5
    },
    flip: {
        flex: 1,
        alignSelf: 'flex-end',
        alignItems: 'center',

    },
    text: {
        fontSize: 18,
        marginBottom: 10,
        color: 'red'
    },
    img_logo: {
        width: 100,
        resizeMode: 'contain'
    },
    flip_cam: {
        color: '#FF0000',
        fontSize: 20,
        textAlign: 'center',
        textAlignVertical: 'center',
        flex: 1
    },
    but_cam: {
        backgroundColor: '#2063AB',
        alignItems: 'center',
        borderRadius: 40,
        justifyContent: 'center',
        width:'70%',
        bottom:'15%',
        position:'absolute'
    },
    but_text: {
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: 5,
        padding: 10,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    but_scan:{
        backgroundColor: '#2063AB',
        alignItems: 'center',
        borderRadius: 40,
        justifyContent: 'center',
        width:'70%',
        top:'10%',
        position:'absolute'
    },
    but_back:{
        backgroundColor: 'transparent',
        alignItems: 'center',
        borderRadius: 40,
        justifyContent: 'center',
        borderWidth:2,
        borderColor:'#2063AB',
        width:'30%',
        bottom:'2%',
        left:'2%',
        position:'absolute'
    },
    but_check:{
        backgroundColor: 'transparent',
        alignItems: 'center',
        borderRadius: 40,
        justifyContent: 'center',
        borderWidth:2,
        borderColor:'#2063AB',
        width:'30%',
        bottom:'2%',
        right:'2%',
        position:'absolute'
    }
});

export default styles;