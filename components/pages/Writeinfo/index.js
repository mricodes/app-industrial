import React, { useState, useEffect, Component } from 'react';
import { Text, View, Image, TouchableOpacity, TextInput, Keyboard, Animated } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from './styles'

let character_Pos = 0;
let barcode_info = '';
let analyst_info = 0;
let qrcode_info = '';
const barcode_Key = 'BARCODE_KEY';
const qrcode_Key = 'QRCODE_KEY';
const analyst_Key = 'ANALYST_KEY';
let Info = [];




const images = {
    char: {
        a0: require('../../../src/images/AutoLab_generic.png'),
        a1: require('../../../src/images/config.png'),
        a2: require('../../../src/images/sample.png'),
    }
}
const image_list = [
    images.char.a0,
    images.char.a1,
    images.char.a2,
]

const storeData = async (key, value) => {
    try {
        await AsyncStorage.setItem(key, value)
        console.log(`saveData: ${value}`)
    } catch (e) {
        // saving error
    }
}


class WriteinfoScreen extends Component {
    state = {
        focusbutton:0,
        backbutton:0,
        idinput:0,
        barcodeinput:0,
        qrcodeinput:0
    }
    render() {
        const { navigation } = this.props;
        return (
            <Animated.View style={styles.container}>
                <Image style={styles.img_logo}
                    source={image_list[0]}
                ></Image>
                <Text style={styles.titleinput}>Id Analista</Text>
                 <TextInput style={styles.textinput}
                    placeholder='Preencha com o id do Analista'
                    disableFullscreenUI={true}
                    keyboardType='default'
                    onSubmitEditing={Keyboard.dismiss}></TextInput>
                <Text style={styles.titleinput}>Código de Barras</Text>
                <TextInput style={styles.textinput}
                    placeholder='Preencha com o id da amostra'
                    disableFullscreenUI={true}
                    keyboardType='default'
                    onSubmitEditing={Keyboard.dismiss}></TextInput>

                <Text style={styles.titleinput}>QRCODE</Text>
                <TextInput style={styles.textinput}
                    placeholder='Preencha com o id da amostra'
                    disableFullscreenUI={true}
                    keyboardType='default'
                    onSubmitEditing={Keyboard.dismiss}></TextInput>

                <TouchableOpacity style={styles.but_back} activeOpacity={.2} onPress={() => navigation.navigate('Camera')}>
                    <Text style={styles.but_text}>Voltar</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.but_confirm} activeOpacity={.2}>
                    <Text style={styles.but_text}>Confirmar</Text>
                </TouchableOpacity>

                <View style={{ height: 60 }} />
            </Animated.View>
        )
    }
}


export default WriteinfoScreen;