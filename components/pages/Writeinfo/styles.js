import { StyleSheet, Dimensions } from 'react-native';

const window = Dimensions.get('window');

export const IMAGE_HEIGHT = window.width / 2;
export const IMAGE_HEIGHT_SMALL = window.width / 7;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    img_logo: {
        width: 300,
        height: IMAGE_HEIGHT,
        marginTop:20,
        resizeMode: 'contain'
    },
    titleinput: {
        color: '#2063AB',
        fontSize: 20,
        fontWeight: 'bold',
    },
    textinput: {
        borderWidth: 4,
        borderColor: '#2063AB',
        borderRadius: 20,
        padding: 5,
        textAlign: 'center',
        textAlignVertical: 'center',
        marginBottom:10,
        width: window.width - 80
    },
    but_text: {
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: 5,
        padding: 10,
        textAlign: 'center',
        textAlignVertical: 'center'

    },
    but_back: {
        backgroundColor: '#2063AB',
        alignItems: 'center',
        borderRadius: 40,
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#2063AB',
        width: '30%',
        position: 'absolute',
        left: '4%',
        bottom: '10%'
    },
    but_confirm: {
        backgroundColor: '#2063AB',
        alignItems: 'center',
        borderRadius: 40,
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#2063AB',
        width: '30%',
        position: 'absolute',
        right: '4%',
        bottom: '10%'
    },
    but_text: {
        color: '#FFF',
        fontSize: 15,
        fontWeight: 'bold',
        padding: 10,
        textAlign: 'center',
        textAlignVertical: 'center'

    },
});

export default styles;